<img src="readme-assets/logo_waveweb.jpg" alt="WaveWeb" style="width: 250px;"/>

# Workshop Laravel
Esse repositório é sobre o Workshop que fizemos sobre o framework [Laravel](http://laravel.com).

## 1) Objetivos deste Repositório

Conforme já foi dito durante o workshop, o objetivo aqui é dar continuidade ao projeto que estávamos fazendo dentro do workshop. Começamos com um exercício sobre uma escola com as seguintes entidades:

* Estudante (Student)
* Sala de Aula (Classroom)

O objetivo aqui é ampliar esse projeto para podermos avaliar o código de cada candidato.

## 2) Tasks desejadas
As seguintes tasks são desejadas para o projeto:

* Adicionar ao banco de dados uma tabela para "escolas"
* Fazer as telas que realizem o CRUD (Create/Read/Update/Delete) das entidades, inclusive de relacionamento. Exemplo: ao cadastrar um Estudante, informar a Sala de Aula a que ele deve fazer parte. Ao cadastrar uma Escola, informar quais Salas de Aula essa Escola deve ter. 

## 3) Pré-condições:
Seguir as regras no [Manual de Desenvolvimento para Laravel da WaveWeb](https://docs.google.com/document/d/1wLATM_exf-6P5JATf-C--W8J24uxDOjAgQ7pBucqbIU/edit?usp=sharing).

Manter coesão e código limpo. Faça o melhor código que você puder.
Algumas dicas:
* Nunca use declarações "switch/case"
* Evite o uso de "ELSE" em classes (liberado em views BLADE)
* Faça bom uso das rotas e procure URLs semânticas e amigáveis
* Separe bem os conteúdos dos Controllers e suas responsabilidades. Inclusive alterando os Controllers existentes, que muitas coisas fizemos "na pressa" para adiantar no workshop.
* Comente seu código sempre que achar necessário, para explicar algum ponto de vista ou defender uma ideia/abordagem

## 4) Repositório GIT: faça um FORK
Crie um usuário no site [GitLab](http://gitlab.com) e faça um FORK do projeto. Faça commits constantes e significativos. Escreva boas mensagens para cada commit. Isso também será avaliado. Repositórios com um único commit do tipo "fiz tudo aí, pode olhar" serão desconsiderados.

Ao término, envie um e-mail para pedropaulo[at]waveweb.com.br com a URL do seu Fork para avaliação. Vamos avaliar cada caso e dar uma resposta. Mesmo que demore um pouquinho. Aqui na WaveWeb estaremos fora do Brasil entre os dias 13 e 23 de Novembro. Depois disso já vamos começar a dar uma olhada nos resultados.

## 5) E se eu tiver uma dúvida?
Nós não temos um plantão com muito tempo aqui na WaveWeb para tirar dúvidas de todos, mas se você se encontrar em um beco sem saída e uma boa lida na documentação do [Laravel](https://laravel.com/docs/5.5) não resolver e uma boa pesquisada no [Laracast](https://laracasts.com) também não, pode endereçar para mim um e-mail que vou tentar te ajudar.

## 6) Obrigado
Quero agradecer a todos que fizeram parte do Workshop. Foi muito legal. Esperamos ter conseguido passar algum conteúdo ou despertar alguma curiosidade sobre o tema/framework.