<?php

// app/Http/Controllers

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student;
use App\Classroom;

class HomeController extends Controller
{
    public function index(Request $request, $user_id) {
    	// $classroom = ClassRoom::create([
    	// 	"name" => "M220",
    	// 	"shift" => "2",
    	// 	"id" => 30
    	// ]);

    	$studend = Student::create([
    		"classroom_id" => 1,
    		"name" => "João",
    		"uuid" => "A320-TI-2017"
    	]);

    	return view("home", [
    		"user_name" => "Pedro Paulo",
    		"user_id" => $user_id,
    		"classroom" => Classroom::find(1),
    	]);
    }

    public function list() {
    	$student = Student::with('classroom')->find(1);
    	return view('student', ["student" => $student]);
    }
}
