@extends("layout.master")

@section("title")
	@parent - Home Page
@endsection

@section("main-content")
	<div class="row">
		<div class="col-xs-12">
			<h1>Workshop Laravel</h1>
			<p>Conteúdo</p>
			<p>Olá, {{$student->name}}</p>
			<p>Seu id é: {{$student->id}}</p>
			<p>A sua classe é:</p>
			<ul>
				<li>{{$student->classroom->id}}</li>
				<li>{{$student->classroom->full_name}}</li>
				<li>{{$student->classroom->shift}}</li>
			</ul>
		</div>
	</div>
</div>
@endsection