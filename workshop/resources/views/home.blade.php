@extends("layout.master")

@section("title")
	@parent - Home Page
@endsection

@section("main-content")
	<div class="row">
		<div class="col-xs-12">
			<h1>Workshop Laravel</h1>
			<p>Conteúdo</p>
			<p>Olá, {{$user_name}}</p>
			<p>Seu id é: {{$user_id}}</p>
			<p>A classe criada foi:</p>
			<ul>
				<li>{{$classroom->id}}</li>
				<li>{{$classroom->name}}</li>
				<li>{{$classroom->shift}}</li>
			</ul>
		</div>
	</div>
</div>
@endsection